package com.temp.login.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import com.temp.common.service.SessionService;
import com.temp.mapper.LoginAccountMapper;

import jakarta.persistence.NonUniqueResultException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;

@Service
public class LoginService {

    @Autowired
    LoginAccountMapper loginAccountMapper;

    @Autowired
    SessionService sessionService;

    public String getLogin() {
        return "main/_main";
    }

    public void tryLogin(HttpServletRequest request, Model model, HttpSession session) {

        List<Map<String, Object>> findById = loginAccountMapper.findById(request.getParameter("id"));
        List<Map<String, Object>> findByPw = loginAccountMapper.findByPw(request.getParameter("pw"));

        System.out.println("===============");
        System.out.println("\"" + request.getParameter("id") + "\"" + " 계정으로 로그인 시도");
        System.out.println("===============");

        try {
            if (!findByPw.isEmpty() && !findById.isEmpty()) {

                SessionService.sessionId = session.getId(); // 브라우저 세션 값을 서버측에 저장

                session.setMaxInactiveInterval(600); // 세션 유지시간 60초

                System.out.println("JSESSIONID2: " + SessionService.sessionId);
                System.out.println("LOGIN STATUS::  OK");

                model.addAttribute("loginStatus", "1");
                model.addAttribute("msg_1", "로그인에 성공하였습니다.");
            } else if (findById.isEmpty() || findByPw.isEmpty()) {
                System.out.println("LOGIN STATUS::  FAIL: 존재하지 않는 계정으로 접속 시도");

                model.addAttribute("loginStatus", "2");
                model.addAttribute("msg_2", "존재하지 않는 계정입니다.\\n\\nID 혹은 PW를 확인해주세요.");
            }
        } catch (NullPointerException ne) { // 일치하는 계정 정보를 찾을 수 없을 때
            if (findById == null) {
                System.out.println("일치하는 ID를 찾을 수 없습니다.\n" + ne);
            } else if (findByPw == null) {
                System.out.println("일치하는 암호를 찾을 수 없습니다.\n" + ne);
            }
        } catch (ClassCastException cce) {
            System.out.println("알 수 없는 ERROR 발생: " + cce);
        } catch (NonUniqueResultException nure) {
            System.out.println("알 수 없는 ERROR 발생: " + nure);
        } catch (IncorrectResultSizeDataAccessException irsdae) {
            System.out.println("알 수 없는 ERROR 발생: " + irsdae);
        } catch (Exception e) {
            System.out.println("알 수 없는 ERROR 발생: " + e);
        }
    }
}
