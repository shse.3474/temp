package com.temp.register.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.temp.register.service.RegisterService;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@Controller
public class RegisterController {
    @Autowired
    RegisterService registerService;

    @GetMapping(path = "/register")
    public String routeRegister(HttpServletRequest request, Model model) {
        return "register";
    }

    /**
     * 회원가입 로직
     */
    @PostMapping("/register")
    public void submitRegister(HttpServletRequest request, HttpServletResponse response, Model model) {
        registerService.submitRegister(request, response, model);
    }
}
