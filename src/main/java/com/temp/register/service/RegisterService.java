package com.temp.register.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import com.temp.mapper.RegisterAccountMapper;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@Service
public class RegisterService {

    /**
     * 회원가입 로직
     */
    @Autowired
    RegisterAccountMapper registerAccountMapper;

    public void submitRegister(HttpServletRequest request, HttpServletResponse response, Model model) {
        String name = request.getParameter("name");
        String id = request.getParameter("id");
        String pw = request.getParameter("pw");
        String rePw = request.getParameter("re-pw");
        String email = request.getParameter("email");
        LocalDateTime date = LocalDateTime.now();

        List<Map<String, Object>> findById = registerAccountMapper.findById(id);
        int findByEmailCount = registerAccountMapper
                .findByEmailCount(email);

        System.out.println(findById.get(0).isEmpty());

        if (findById.isEmpty()) {
            System.out.println("이미 존재하는 아이디입니다.");
            model.addAttribute("status", "2");
        } else if (id.length() <= 7 || id.length() > 21) {
            System.out.println("ID 최소 규칙은 8자 이상, 20자 이하입니다.\n생성 규칙에 맞게 다시 진행해주세요.");
        } else if (!pw.equals(rePw)) {
            System.out.println("암호와 암호 재입력 부분이 일치하지 않습니다.");
        } else {
            try {
                System.out.println("###############  REGISTER STATUS:: TRY  ###############");
                registerAccountMapper.insertByAccount(name, id, pw, email, date);

                model.addAttribute("status", "1");

                response.sendRedirect("/");

                System.out.println("###############  REGISTER STATUS:: OK  ###############");
            } catch (Exception e) {
                System.out.println(e);
            } finally {
                System.out.println(findByEmailCount);

            }
        }

        // return "register";
    }
}
