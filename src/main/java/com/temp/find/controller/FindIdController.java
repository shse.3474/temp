package com.temp.find.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.temp.find.service.FindIdService;

import jakarta.servlet.http.HttpServletRequest;

@Controller
public class FindIdController {

    @Autowired
    FindIdService findIdService;

    @GetMapping("/findAccount/findId")
    public String routeFindId() {
        return "/findAccount/findId";
    }

    @GetMapping("/findAccount/findIdSuccecs")
    public String routeFindIdSuccecs() {
        return "/findAccount/findIdSuccecs";
    }

    @PostMapping("/findAccount/findId")
    public String findId(HttpServletRequest request, Model model) {
        findIdService.findId(request, model);
        return "/findAccount/findIdSuccecs";
    }
}
