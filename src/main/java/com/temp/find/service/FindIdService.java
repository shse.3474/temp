package com.temp.find.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import com.temp.mapper.FindAccountMapper;

import jakarta.servlet.http.HttpServletRequest;

@Service
public class FindIdService {

    @Autowired
    FindAccountMapper findAccountMapper;

    public void findId(HttpServletRequest request, Model model) {
        String name = request.getParameter("name");
        String email = request.getParameter("email");

        try {
            List<Map<String, Object>> result = findAccountMapper.getFindIdAndCreateDate(name, email);
            String createDate = result.get(0).get("CREATE_DATE").toString().substring(0, 10);
            String id = result.get(0).get("ID").toString();

            model.addAttribute("status", 1);
            model.addAttribute("createDate", createDate);
            model.addAttribute("id", id);
        } catch (IndexOutOfBoundsException e) {
            System.out.println("존재하는 계정이 없어 예외 처리 되었습니다.");
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
