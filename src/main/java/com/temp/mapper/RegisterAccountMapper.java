package com.temp.mapper;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

// import java.security.Timestamp;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface RegisterAccountMapper {
    /* 회원가입 */
    void insertByAccount(@Param("name") String name, @Param("id") String id,
            @Param("pw") String pw,
            @Param("email") String email, @Param("date") LocalDateTime date);

    List<Map<String, Object>> findById(@Param("id") String id);

    int findByEmailCount(@Param("email") String email);
}
