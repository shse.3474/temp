package com.temp.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface LoginAccountMapper {
    List<Map<String, Object>> findById(@Param("id") String id);

    List<Map<String, Object>> findByPw(@Param("pw") String pw);
}
