package com.temp.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface FindAccountMapper {
    List<Map<String, Object>> getFindIdAndCreateDate(@Param("name") String name, @Param("email") String email);

    List<Map<String, Object>> getRegister(@Param("name") String name, @Param("id") String id,
            @Param("pw") String pw, @Param("email") String email);
}
