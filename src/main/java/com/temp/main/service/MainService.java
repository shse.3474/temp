package com.temp.main.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import com.temp.common.service.SessionService;
import com.temp.mapper.FindAccountMapper;
import com.temp.mapper.RegisterAccountMapper;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;

@Service
public class MainService {

    @Autowired
    SessionService sessionService;

    @Autowired
    FindAccountMapper findAccountMapper;

    @Autowired
    RegisterAccountMapper registerAccountMapper;

    public String getMain(HttpServletRequest request, HttpSession session, Model model) {
        sessionService.expireSession(session, model);
        model.addAttribute("status", SessionService.sessionId);
        model.addAttribute("sessionId", session.getId());

        return "main/main";
    }
}
