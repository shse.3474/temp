package com.temp.main.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.temp.main.service.MainService;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;

@Controller
public class MainController {

    @Autowired
    MainService mainService;

    @GetMapping("/")
    public String getMain(HttpServletRequest request, HttpSession session, Model model) {
        return mainService.getMain(request, session, model);
    }
}
