package com.temp.common.service;

import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import jakarta.servlet.http.HttpSession;

@Service
public class SessionService {
    /* 세션 값 */
    public static String sessionId = null;

    public void expireSession(HttpSession session, Model model) {
        if (!session.getId().equals(sessionId) || sessionId == null) { // 브라우저 저장 된 세션과 기존 저장 된 세션 값이 불일치 경우
            System.out.println("세션이 없습니다.");
            session.invalidate();
        } else if (session.getId().equals(sessionId)) {
            System.out.println("세션이 존재합니다.");
            model.addAttribute("sessionId", session.getId());
        }
    }
}
