<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<!DOCTYPE html>
<html lang="ko">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>ID 조회 목록</title>

        <link rel="stylesheet" href="/css/findAccount/findIdSuccecs.css">
    </head>

    <body>
        <header></header>

        <main>
            <div>
                <div id="findIdText">ID 찾기</div>
                <div id="findIdText2">입력하신 정보와 일치하는 ID는 아래와 같습니다.</div>
            </div>
            <br>
            <div id="find">
                <c:choose>
                    <c:when test="${id ne null and createDate ne null}">
                        <div>아이디: ${id}</div>
                        <div>가입 일자: ${createDate}</div>
                    </c:when>
                    <c:otherwise>
                        입력하신 정보에 대해 존재하는 ID가 없습니다.
                    </c:otherwise>
                </c:choose>
            </div>
            <br>
            <div id="findPw">
                <a href="/findAccount/findPw" id="goFindPw">비밀번호 찾기</a>
            </div>
        </main>


        <footer></footer>
    </body>

</html>
