<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<!DOCTYPE html>
<html lang="ko">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>findID</title>

        <link rel="stylesheet" href="/css/findAccount/findId.css">
    </head>

    <body>
        <header></header>
        <main>
            <div>
                <div id="parentsForm">
                    <form action="/findAccount/findId" method="post">
                        <input type="text" placeholder="성명" name="name">
                        <br><br>
                        <input type="email" placeholder="이메일" name="email">
                        <br><br>
                        <input type="submit" value="찾기">
                    </form>
                </div>
            </div>
        </main>
        <footer></footer>

        <script>
            if ("${status}" === '1') {
                location.href = "/findAccount/findIdSuccecs";
                console.log("성공");
                alert("성공");

            } else if ("${status}" === '0') {
                console.log("실패");
            }
        </script>
    </body>

</html>
