<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<!DOCTYPE html>
<html lang="ko">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>login</title>

        <link rel="stylesheet" href="/css/main/_main.css">

        <script src="https://code.jquery.com/jquery-latest.min.js"></script>
    </head>

    <body>
        <header></header>
        <main>
            <div id="title">
                <div>COMMUNITY</div>
                <div>LOGIN</div>
            </div>
            <div id="tempLoginForm">
                <form id="tempInputAccount" action="/main/_main" method="post">
                    <div class="temp-input">
                        <div class="input-text">아이디</div>
                        <br>
                        <input class="temp-input-2" type="text" placeholder="I  D" name="id">
                    </div>
                    <div class="temp-input">
                        <div class="input-text">암호</div>
                        <br>
                        <input class="temp-input-2" type="password" placeholder="P  W" name="pw">
                    </div>
                    <input id="tempLogin" class="btn-temp-input" type="submit" value="로그인">
                    <div id="findAccountArea">
                        <a href="/register" class="link-bind">회원가입</a>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="/findAccount/findId" class="link-bind">ID찾기</a>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="/" class="link-bind">PW찾기</a>
                    </div>
                </form>
            </div>
        </main>
        <footer></footer>
    </body>
    <script>
        var loginStatus = "${loginStatus}";
        var msg_1 = "${msg_1}";  // 성공
        var msg_2 = "${msg_2}";  // 실패

        if (loginStatus == 1) {
            alert(msg_1);
            location.href = "/";
        } else if (loginStatus == 2) {
            alert(msg_2);
            // location.href = "/register";
        }


        document.getElementById("tempGoRegister").addEventListener("click", () => {
            location.href = "/register";
        });
    </script>

</html>
