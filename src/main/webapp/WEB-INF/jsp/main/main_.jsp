<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>



<!DOCTYPE html>
<html lang="ko">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>main</title>

        <link rel="stylesheet" href="/css/main/main_.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper@8/swiper-bundle.min.css" />

        <script src="https://code.jquery.com/jquery-latest.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/swiper@8/swiper-bundle.min.js"></script>


    </head>

    <body>
        <header></header>
        <main>
            <div class="swiper">
                <div class="swiper-wrapper">
                    <div class="swiper-slide" id="pageOne">
                        <!-- <img src="/img/main.jpg" alt="이미지가 없습니다." id="pageOneMainImg"> -->
                        <div id="pageOneMainText">
                            무등 전착 회사 메인 페이지
                        </div>
                    </div>
                    <div class="swiper-slide" id="pageTwo">
                        <div id="pageTwoMainText">
                            무등 전착 회사 소개 페이지
                        </div>
                    </div>
                    <div class="swiper-slide">PAGE 3</div>
                    <div class="swiper-slide">PAGE 4</div>
                </div>
                <div class="swiper-scrollbar"></div>
            </div>
        </main>
        <footer></footer>


        <script>
            const swiper = new Swiper('.swiper', {
                direction: 'vertical',
                loop: false,
                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                },
                scrollbar: {
                    el: '.swiper-scrollbar',
                },
                mousewheel: {
                    invert: false,
                },
            });
        </script>
    </body>

</html>
