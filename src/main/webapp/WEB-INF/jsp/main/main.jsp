<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<c:choose>
    <c:when test="${sessionId ne status}">
        <%@ include file="/WEB-INF/jsp/main/_main.jsp" %>
    </c:when>
    <c:when test="${sessionId eq status}">
        <%@ include file="/WEB-INF/jsp/main/main_.jsp" %>
    </c:when>
    <c:otherwise>
        예상못한 변수
    </c:otherwise>
</c:choose>
