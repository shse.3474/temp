<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>회원 가입</title>

        <link rel="stylesheet" href="/css/registerStyle.css">

        <script src="https://code.jquery.com/jquery-latest.min.js"></script>
    </head>

    <body>
        <div id="tempRegisterForm">
            <div id="tempRegisterForm2">
                <form id="tempInput" action="/register" method="post">
                    <input class="tempInput" type="text" placeholder="Name" name="name" required>
                    <br><br>
                    <input class="tempInput" type="text" placeholder="ID" name="id" required>
                    <br><br>
                    <input class="tempInput" type="password" placeholder="Password" name="pw" required>
                    <br><br>
                    <input class="tempInput" type="password" placeholder="Re-enter password" name="re-pw" required>
                    <br><br>
                    <input class="tempInput" type="email" placeholder="E-Mail" name="email" required>
                    <br><br>
                    <input class="tempInput" id="tempInputSubmit" type="submit" value="SUBMIT">
                    <br><br>
                </form>
            </div>
        </div>
        <script>
            if ("${status}" == 1) {
                window.open("http://www.naver.com", "asdad", "width=850px, height=850px");
            } else if ("${status}" == 2) {
                alert("암호와 재입력 암호가 일치하지 않습니다.");
            } else if ("${status}" == 3) {
                alert("이메일 중복 에러");
            } else if ("${status}" == 4) {
                alert("아이디 중복 에러");
            }
        </script>
    </body>

</html>
